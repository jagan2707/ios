# Coding Challenge for iOS
The current application is rendering 5000 photo records from one api service , each photo record should be clickable which will display the photo record's image.

### Getting started
Clone this repository and open the Xcode project.
```sh
$ ios git clone https://code-challenge-accenture@bitbucket.org/code-challenge-accenture/ios.git
$ ios open ios_code_challenge.xcodeproj/
```

### 1. Bugs
- Photo records have been loaded but not displayed.
- Tapping on a photo record should go to a detail view and display its image.

There are a few more bugs, please go ahead and fix them on your own free will.

### 2. Performance fix
- The table view flickers when displaying the photo records. Optimize the rendering of the table view and implement pagination.

Please go ahead and contribute further improvements on your own free will.

### 3. Architecture - Codebase optimization & maintainability
- Implement a better architecture (e.g. MVVM, MVC).
- Give the ViewController Class a single responsibility.

Please go ahead and contribute further improvements on your own free will.

### 4. Unit tests
- Unit tests have not been added. Please add unit tests.

### 5. New feature
- Tapping the photo record displays the photo in a detail view. Bonus points if you don't "push" a new view to display the detail view.
- User can search for a specific photo record by typing its title in the Search Bar.
- Add sounds or any improvements to make the user experience more engaging.

### How to submit

As a deliverable, please clone the project, proceed with your code commits according to the below format for each commit comment. Once your done, please notify us. Enjoy.

```
- [known bug]: text 
- [unknown bug]: text 
- [known performance fix]: text
- [unknown performance fix]: text
- [known architecture fix]: text
- [unknown architecture fix]: text
- [unit tests]: text
- [new feature]: text
```
