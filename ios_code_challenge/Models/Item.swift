//
//  Item.swift
//  ios_code_challenge
//
//  Created by Chartchai Pothong on 29/01/2019.
//  Copyright © 2019 Accenture. All rights reserved.
//

import Foundation

public struct Item {
  let albumID: Int
  let photoID: Int
  let title: String
  let photoURL: String
  let thumbnailURL: String
  
  // MARK: - Initializers
  init?(json: [String: Any]) {
    guard
      let albumID = json["albumId"] as? Int,
      let photoID = json["id"] as? Int,
      let title = json["title"] as? String,
      let photoURL = json["url"] as? String,
      let thumbnailURL = json["thumbnailUrl"] as? String
      else {
        return nil
    }
    self.albumID = albumID
    self.photoID = photoID
    self.title = title
    self.photoURL = photoURL
    self.thumbnailURL = thumbnailURL
  }
  
}
