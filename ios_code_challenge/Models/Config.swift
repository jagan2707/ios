//
//  Config.swift
//  ios_code_challenge
//
//  Created by Chartchai Pothong on 30/01/2019.
//  Copyright © 2019 Accenture. All rights reserved.
//

import Foundation

enum Photo
{
  enum GetPhotoList {
    struct Request
    {
      let url = "http://jsonplaceholder.typicode.com/photos"
    }
    struct Response
    {
      var results : [Item]
    }
  }
  
}

public enum ReturnError: Error
{
  case invalidJson
  case networkError
}
