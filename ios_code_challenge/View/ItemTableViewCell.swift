//
//  ItemTableViewCell.swift
//  ios_code_challenge
//
//  Created by Chartchai Pothong on 30/01/2019.
//  Copyright © 2019 Accenture. All rights reserved.
//

import UIKit

class ItemTableViewCell: UITableViewCell {
  
  @IBOutlet weak var photoImageView: UIImageView!
  @IBOutlet weak var photoTitle: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
