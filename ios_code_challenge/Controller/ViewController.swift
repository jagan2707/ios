//
//  ViewController.swift
//  ios_code_challenge
//
//  Created by John Louis on 6/12/17.
//  Copyright © 2017 Accenture. All rights reserved.
//

import UIKit
import SDWebImage
import AudioToolbox

class PhotoListController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    var apiManger = APIManager()
    var resultItems = Array<Item>()
    var items = Array<Item>()
    var loadingView: LoadingView?
    var cellActive : Bool = false
    var currentSelection = 0
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
     
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.setupTableView()
        self.getPhotoList()
    }
    
    // MARK: - Setup
    func setupTableView() {
// MARK: [new scope]: Selecting on a cell expands the cell and displays an image
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
    }
  
  //MARK:Get Photo List
  func getPhotoList() {

    if (!Reachability.isConnectedToNetwork()){
      self.showNetworkAlert()
      return
    }
    self.addActivityIndicator()
    let request = Photo.GetPhotoList.Request()
    apiManger.getPhotoList(request: request, onSuccess: {  result in
      self.items = result
      self.resultItems = result
      self.displayData()
    }, onFailure: { (error) in
      
      DispatchQueue.main.async {
        
        self.removeActivityIndicator()
        self.showAlert(alertString: "Something went wrong")
      }
      
    })

  }

  func displayData()
  {
    DispatchQueue.main.async {
      
      self.removeActivityIndicator()
      self.tableView.reloadData()
    }
  }
  
  //MARK: ShowFailure Aler
  func showAlert(alertString: String) {
    let alert = UIAlertController(title: "Error", message: alertString, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title:"OK", style: .cancel, handler: {(_ action: UIAlertAction) -> Void in
    }))
    self.present(alert, animated: true, completion: {  })
  }
 
  private func showNetworkAlert()  {
    let alert = UIAlertController(title: "No Internet", message: "Please check your device internet connection and try again", preferredStyle: .alert)
    alert.addAction(UIAlertAction(title:"OK", style: .default, handler: nil))
    self.present(alert, animated: true, completion: {  })
  }
  
  //MARK Activity Indicator
  
  private func addActivityIndicator() {
    loadingView = LoadingView(frame:(self.view.bounds))
    self.view.addSubview(loadingView!)
  }
  
  private func removeActivityIndicator() {
    loadingView?.removeFromSuperview()
  }

}

// MARK: - UITableViewDataSource

extension PhotoListController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return resultItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
      let cell: ItemTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ItemCell")! as! ItemTableViewCell
      
// MARK: [known bug]: Display the title of the photo
        let item = resultItems[indexPath.row]
        cell.photoTitle?.text = item.title
     
      cell.photoImageView?.sd_setImage(with: URL(string:item.thumbnailURL), placeholderImage: UIImage(named: "placeholder.png"))
      return cell

    }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
  {
    if (currentSelection == indexPath.row + 1) && cellActive == true  {
      return 150
    }
    else
    {
      return 70
    }
  }
}

//MARK: UITableViewDelegate
extension PhotoListController:UITableViewDelegate
{
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    currentSelection = indexPath.row+1
    cellActive = true
    tableView.beginUpdates()
    tableView.endUpdates()
    
    let url = URL(string: "/System/Library/Audio/UISounds/ReceivedMessage.caf")
    var soundID1 : SystemSoundID = 0
    AudioServicesCreateSystemSoundID(url! as CFURL ,&soundID1)
    AudioServicesPlaySystemSound(soundID1)
  }
}

//MARK: SearchBarDelegate
extension PhotoListController: UISearchBarDelegate
{
  
  
  func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
    if searchBar.text?.count == 0 {
      resultItems = items
      tableView.reloadData()
      return
    }
    resultItems = items.filter({ (item) -> Bool in
      let tmp: NSString = item.title as NSString
      let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
      return range.location != NSNotFound
    })
    cellActive = false
    self.tableView.reloadData()
  }

  public func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
  {
    searchBar.resignFirstResponder()
  }
  
  func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
    searchBar.resignFirstResponder()
  }
}
