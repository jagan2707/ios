//
//  APIManager.swift
//  ios_code_challenge
//
//  Created by Chartchai Pothong on 29/01/2019.
//  Copyright © 2019 Accenture. All rights reserved.
//

import Foundation

class APIManager {
    
  func getPhotoList(request: Photo.GetPhotoList.Request, onSuccess success: @escaping (_ result: [Item]) -> Void, onFailure failure: @escaping (_ error: Error?) -> Void) {
    
    let url = URL(string: request.url)
    URLSession.shared.dataTask(with:url!, completionHandler: {(data, response, error) in
      
      do {
        guard let data = data, error == nil else {
          throw error ?? ReturnError.invalidJson
        }
        if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]]
        {
          let photoListArray = json.map {
            Item(json: $0 )
          }
          success(photoListArray as! [Item])
        }
      } catch let error as NSError {
        
        failure(error)
      }
    }).resume()
  }
  
}
