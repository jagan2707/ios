//
//  ios_code_challengeTests.swift
//  ios_code_challengeTests
//
//  Created by Chartchai Pothong on 31/01/2019.
//  Copyright © 2019 Accenture. All rights reserved.
//

import XCTest
@testable import ios_code_challenge

class ios_code_challengeTests: XCTestCase {

  var photoViewController : PhotoListController!
 
  override func setUp() {
     self.setUpViewController()
    }

    override func tearDown() {
      
    }

  private func setUpViewController()
  {
    photoViewController = PhotoListController()
   // apiManagerSpy = ApiManagerSpy()
  }
  
  class PhotoListViewControllerSpy: PhotoListController
  {
    var apiManagerSpy: ApiManagerSpy?
    var displayList = false
    var showFailureAlert = false
    override func getPhotoList() {
      apiManagerSpy?.getPhotoList(request: Photo.GetPhotoList.Request(), onSuccess: { data in
        self.displayData()
      }) { error in
        self.showAlert(alertString: "Something went wrong")
      }
    }
    
    override func displayData() {
       displayList = true
    }
    
    override func showAlert(alertString: String)
    {
      showFailureAlert = true
    }
  }
  
  class ApiManagerSpy: APIManager {
    
    var getPhotoList = false
    var isFailure = true
    override func getPhotoList(request: Photo.GetPhotoList.Request, onSuccess success: @escaping ([Item]) -> Void, onFailure failure: @escaping (Error?) -> Void) {
      
      getPhotoList = true
      if !isFailure {
        success([Item]())
      }
      else {
        failure(ReturnError.invalidJson)
      }
    }
    
  }
  
  func testGetPhotoListShouldAskApiManagerToLoadData(){
   
    //Given
    let apiManagerSpy = ApiManagerSpy()
    apiManagerSpy.getPhotoList = true
    photoViewController.apiManger = apiManagerSpy
    
    //When
    photoViewController.getPhotoList()
    
    //Then
    XCTAssertTrue(apiManagerSpy.getPhotoList)
  }
  
  func testGetPhotoListShouldAskPhotoListControllerToDisplayList(){
    
    //Given
    let apiManagerSpy = ApiManagerSpy()
    apiManagerSpy.isFailure = false
    let photoViewControllerSpy = PhotoListViewControllerSpy()
    photoViewControllerSpy.apiManagerSpy = apiManagerSpy
    
    //When
    photoViewControllerSpy.getPhotoList()
    
    //Then
    XCTAssertTrue(photoViewControllerSpy.displayList)
  }
  
  func testGetPhotoListShouldAskPhotoListControllerToShowFailreAlert() {
    
    //Given
    let apiManagerSpy = ApiManagerSpy()
    apiManagerSpy.isFailure = true
    let photoViewControllerSpy = PhotoListViewControllerSpy()
    photoViewControllerSpy.apiManagerSpy = apiManagerSpy
    
    //When
    photoViewControllerSpy.getPhotoList()
    
    //Then
    XCTAssertTrue(photoViewControllerSpy.showFailureAlert)
  }
  
}
